package org.make.kafka.copier

import java.util.Properties

import ch.qos.logback.classic.{Level, Logger}
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.StrictLogging
import org.apache.kafka.clients.admin.{AdminClient, AdminClientConfig, NewTopic}
import org.apache.kafka.streams.{KafkaStreams, StreamsConfig}
import org.slf4j.LoggerFactory

import scala.collection.JavaConverters.seqAsJavaListConverter

object Main extends App with StrictLogging {

  def ccToMap(cc: AnyRef) =
    (Map[String, Any]() /: cc.getClass.getDeclaredFields) {
      (a, f) =>
        f.setAccessible(true)
        a + (f.getName -> f.get(cc))
    }

  val configuration = ConfigFactory.load()

  private val streamConfiguration = {
    val source: String = configuration.getString("kafka-copier.source")
    val destination: String = configuration.getString("kafka-copier.destination")
    val tmp: String = {
      val fromConf: String = configuration.getString("kafka-copier.tmp")
      if (fromConf == "") s"tmp-$destination" else fromConf
    }
    val registryUrl: String = configuration.getString("kafka-copier.registry-url")
    val range: Long = configuration.getLong("kafka-copier.range")
    val threads: Int = configuration.getInt("kafka-copier.threads")

    StreamConfiguration(source, destination, tmp, registryUrl, range, threads)
  }

  val properties: Properties = new Properties()
  private val applicationId: String =
    configuration.getString("kafka-copier.application-id")

  private val bootstrapServers: String =
    configuration.getString("kafka-copier.bootstrap-servers")

  properties.put(StreamsConfig.APPLICATION_ID_CONFIG, applicationId)
  properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers)
  properties.put(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG,
    classOf[AvroTimestampExtractor].getName)
  properties.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, streamConfiguration.threads.toString)
  properties.put("fetch.max.bytes", configuration.getString("kafka-copier.fetch-max"))
  properties.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, configuration.getString("kafka-copier.guarantee"))
  properties.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG,
    classOf[ShortLogAndContinueExceptionHandler].getName)

  // Set ROOT level to INFO
  LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME).asInstanceOf[Logger].setLevel(Level.INFO)
  // Disabling RecordDeserializer logging because it duplicates what is logged by the exception handler
  LoggerFactory.getLogger("org.apache.kafka.streams.processor.internals.RecordDeserializer")
    .asInstanceOf[Logger].setLevel(Level.ERROR)

  logger.info("Starting kafka topic copier with parameters:")
  logger.info(s"Application-id: $applicationId")
  logger.info(s"StreamConfiguration: ${pprint.apply(ccToMap(streamConfiguration))}")
  createTopicsIfNeeded

  val topology = new CopyStream(streamConfiguration).topology
  logger.info("Topology description {}", topology.describe());

  val stream = new KafkaStreams(topology, properties)
  if (configuration.getBoolean("kafka-copier.clean-up")) {
    logger.info("Run stream.cleanUp")
    stream.cleanUp()
  }
  stream.start()

  Runtime.getRuntime.addShutdownHook(new Thread(() => stream.close()))

  def createTopicsIfNeeded: Unit = {
    val config: Properties = new Properties()
    config.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);

    val admin = AdminClient.create(config)
    val numPartitions = configuration.getInt("kafka-copier.num-partitions").asInstanceOf[Short]
    val replicationFactor = configuration.getInt("kafka-copier.replication-factor").asInstanceOf[Short]

    val topics = admin.listTopics().names().get()
    val toCreate = Seq(streamConfiguration.tmpTopic, streamConfiguration.destinationTopic).flatMap {
      t => if (!topics.contains(t)) Some(new NewTopic(t, numPartitions, replicationFactor)) else None
    }

    if (toCreate.nonEmpty) {
      logger.info("Creating {}", pprint.apply(toCreate))
      val result = admin.createTopics(toCreate.asJava).all
      result.get()
    }
  }

}