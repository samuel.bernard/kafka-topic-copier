package org.make.kafka.copier

import com.typesafe.scalalogging.StrictLogging
import org.apache.avro.generic.GenericRecord
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Produced
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala.Serdes._
import org.apache.kafka.streams.scala.{Serdes, StreamsBuilder}
import org.apache.kafka.streams.state.Stores

class CopyStream(configuration: StreamConfiguration) extends StrictLogging {
  implicit val avroSerDe: Serde[GenericRecord] = new AvroSerDes(configuration.registryUrl)
  implicit val nullSerDe: Serde[Null] = new NullSerDes()

  def extractHash(value: GenericRecord): Int = {
    Option(value.get("id")).getOrElse(value.get(0)).hashCode()
  }

  private val produced: Produced[Null, GenericRecord] =
    implicitly[Produced[Null, GenericRecord]]
      .withStreamPartitioner {
        (_topic: String, _key: Null, value: GenericRecord, numPartitions: Int) =>
          (extractHash(value) % numPartitions + numPartitions) % numPartitions
      }

  private val builder = new StreamsBuilder()

  // Create a state store manually.
  val sortingStore = Stores.keyValueStoreBuilder(
    Stores.persistentKeyValueStore("sortingStore"),
    Serdes.String, avroSerDe
  ).withLoggingDisabled()
  builder.addStateStore(sortingStore)

  val reorderer = new ReorderingTransformerSupplier(sortingStore.name(), configuration.range)

  builder
    .stream[String, GenericRecord](configuration.sourceTopic)
    .selectKey[Null] { case (_, _) => null }
    .through(configuration.tmpTopic)(produced)
    .transform[Null, GenericRecord](reorderer, sortingStore.name())
    .to(configuration.destinationTopic)(produced)

  def topology: Topology = builder.build()
}

final case class StreamConfiguration(
                                      sourceTopic: String,
                                      destinationTopic: String,
                                      tmpTopic: String,
                                      registryUrl: String,
                                      range: Long,
                                      threads: Int
                                    )
