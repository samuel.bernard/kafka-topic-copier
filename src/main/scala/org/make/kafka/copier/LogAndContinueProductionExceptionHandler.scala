package org.make.kafka.copier

import java.util

import com.typesafe.scalalogging.StrictLogging
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.streams.errors.ProductionExceptionHandler
import org.apache.kafka.streams.errors.ProductionExceptionHandler.ProductionExceptionHandlerResponse

class LogAndContinueProductionExceptionHandler extends ProductionExceptionHandler with StrictLogging {
  override def handle(record: ProducerRecord[Array[Byte], Array[Byte]], exception: Exception):
    ProductionExceptionHandler.ProductionExceptionHandlerResponse = {
    logger.warn("Deserialization error, topic: {}, partition: {}, timestamp: {}",
                 record.topic(), record.partition(), record.timestamp())

    ProductionExceptionHandlerResponse.CONTINUE
  }

  override def configure(configs: util.Map[String, _]): Unit = {}
}
