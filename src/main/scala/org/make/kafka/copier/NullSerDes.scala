package org.make.kafka.copier

import java.util

import org.apache.kafka.common.serialization.{Deserializer, Serde, Serializer}

class NullSerDes extends Serde[Null] {

  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = {}

  override def close(): Unit = {}

  override val serializer: Serializer[Null] = {
    new Serializer[Null] {
      override def configure(configs: util.Map[String, _],
                             isKey: Boolean): Unit = {}

      override def serialize(topic: String,
                             data: Null): Array[Byte] = {
        null
      }

      override def close(): Unit = {}
    }

  }

  override val deserializer: Deserializer[Null] = {
    new Deserializer[Null] {
      override def configure(configs: util.Map[String, _],
                             isKey: Boolean): Unit = {}

      override def deserialize(topic: String,
                               data: Array[Byte]): Null = {
        null
      }

      override def close(): Unit = {}
    }
  }
}
