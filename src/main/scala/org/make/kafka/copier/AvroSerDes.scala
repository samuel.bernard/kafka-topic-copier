package org.make.kafka.copier

import java.util

import com.typesafe.scalalogging.StrictLogging
import io.confluent.kafka.schemaregistry.client.{CachedSchemaRegistryClient, SchemaRegistryClient}
import io.confluent.kafka.serializers.{KafkaAvroDeserializer, KafkaAvroSerializer}
import org.apache.avro.Schema
import org.apache.avro.generic.GenericRecord
import org.apache.kafka.common.errors.SerializationException
import org.apache.kafka.common.serialization.{Deserializer, Serde, Serializer}

import scala.collection.JavaConverters._
import scala.collection.mutable

class AvroSerDes(registryUrl: String) extends Serde[GenericRecord] with StrictLogging {

  private val schemaRegistryClient: SchemaRegistryClient =
    new CachedSchemaRegistryClient(registryUrl, 1000)

  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = {}

  override def close(): Unit = {}

  override val serializer: Serializer[GenericRecord] = {
    new Serializer[GenericRecord] {
      val delegate =
        new KafkaAvroSerializer(
          schemaRegistryClient,
          Map("schema.registry.url" -> registryUrl).asJava)

      override def configure(configs: util.Map[String, _],
                             isKey: Boolean): Unit = {
        delegate.configure(configs, isKey)
      }

      override def serialize(topic: String,
                             data: GenericRecord): Array[Byte] = {
        delegate.serialize(topic, data)
      }

      override def close(): Unit = {
        delegate.close()
      }
    }

  }

  override val deserializer: Deserializer[GenericRecord] = {
    new Deserializer[GenericRecord] {
      val delegate = new KafkaAvroDeserializer(schemaRegistryClient)
      val cache = new mutable.HashMap[String, Schema]()

      override def configure(configs: util.Map[String, _],
                             isKey: Boolean): Unit = {
        delegate.configure(configs, isKey)
      }

      override def deserialize(topic: String,
                               data: Array[Byte]): GenericRecord = {
        val schema = latestSchema(topic)
        val original = try { // try with original schema
          delegate.deserialize(topic, data).asInstanceOf[GenericRecord]
        } catch {
          case e: Throwable =>
            throw new SerializationException(s"${e.getMessage}\nCannot deserialize with original schema")
        }

        try { // if it works, try with last schema to test backward compatibility
          delegate.deserialize(topic, data, schema).asInstanceOf[GenericRecord]
        } catch {
          case e: SerializationException =>
            throw new SerializationException(
              s"${e.getMessage}\nTry to read ${original.getSchema.getFullName} with ${schema.getFullName}"
            )
        }
        // throw new SerializationException("mouhahhah")
      }

      override def close(): Unit = {
        delegate.close()
      }

      def latestSchema(topic: String): Schema = {
        cache.getOrElseUpdate(topic, {
          val subject = s"$topic-value"
          val metadata = schemaRegistryClient.getLatestSchemaMetadata(subject)
          schemaRegistryClient.getBySubjectAndId(subject, metadata.getId)
        })
      }
    }
  }
}
