package org.make.kafka.copier

import java.util

import com.typesafe.scalalogging.StrictLogging
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.streams.errors.DeserializationExceptionHandler
import org.apache.kafka.streams.errors.DeserializationExceptionHandler.DeserializationHandlerResponse
import org.apache.kafka.streams.processor.ProcessorContext

import scala.collection.mutable

class ShortLogAndContinueExceptionHandler extends DeserializationExceptionHandler with StrictLogging {
  val alreadySeen = new mutable.HashSet[String]()

  override def handle(context: ProcessorContext,
                      record: ConsumerRecord[Array[Byte], Array[Byte]],
                      exception: Exception): DeserializationExceptionHandler.DeserializationHandlerResponse = {
    val message = exception.getMessage
    if (!alreadySeen.contains(message)) {
      logger.warn("Deserialization error, taskId: {}, topic: {}, partition: {}, offset: {}\nexception: {}",
        context.taskId(), record.topic(), record.partition(), record.offset(), message)
      alreadySeen.add(message)
    }
    DeserializationHandlerResponse.CONTINUE
  }

  override def configure(configs: util.Map[String, _]): Unit = {}
}
