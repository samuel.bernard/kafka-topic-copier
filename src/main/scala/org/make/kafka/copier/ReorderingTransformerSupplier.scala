package org.make.kafka.copier

import java.lang.management.ManagementFactory
import java.time.Duration

import com.typesafe.scalalogging.StrictLogging
import javax.management.{MBeanServer, ObjectName}
import org.apache.avro.generic.GenericRecord
import org.apache.kafka.common.Metric
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.kstream.{Transformer, TransformerSupplier}
import org.apache.kafka.streams.processor.{ProcessorContext, PunctuationType, Punctuator, To}
import org.apache.kafka.streams.state.KeyValueStore

import scala.collection.JavaConverters.{iterableAsScalaIterableConverter, mapAsScalaMapConverter}
import scala.collection.mutable

class ReorderingTransformerSupplier(storeName: String, initialRange: Long, marginFactor: Int = 10)
  extends TransformerSupplier[Null, GenericRecord, KeyValue[Null, GenericRecord]] with StrictLogging {

  override def get(): Transformer[Null, GenericRecord, KeyValue[Null, GenericRecord]] = {
    new Transformer[Null, GenericRecord, KeyValue[Null, GenericRecord]] {
      var store: KeyValueStore[String, GenericRecord] = null
      val queue = new mutable.PriorityQueue()(Message.ordering)
      val mBeanServer: MBeanServer = ManagementFactory.getPlatformMBeanServer()
      var context: ProcessorContext = null
      var range: Long = initialRange
      var processRateMetric: Option[(Metric)] = null
      var lagRefs: Iterable[ObjectName] = null
      var processed = 0L
      var lastAdaptLog = ""
      var started = false

      override def init(context: ProcessorContext): Unit = {
        this.context = context
        this store = context.getStateStore(storeName).asInstanceOf[KeyValueStore[String, GenericRecord]]
        rateMetric()
        setLagRefs()
        context.schedule(
          Duration.ofSeconds(30L),
          PunctuationType.WALL_CLOCK_TIME,
          { case t => adaptRange(t) }: Punctuator
        )
      }

      def rateMetric(): Unit = {
        val metrics = context.metrics().metrics().asScala
        this.processRateMetric = metrics.find {
          case (metric, _) =>
            metric.group() == "stream-metrics" &&
              metric.name() == "process-rate"
        }.flatMap { c => Option(c._2) }
      }

      def setLagRefs(): Unit = {
        val appId = context.appConfigs().get("application.id")
        val mON = new ObjectName(s"kafka.consumer:type=consumer-fetch-manager-metrics,client-id=$appId-*-consumer")
        val instances = mBeanServer.queryNames(mON, null)
        this.lagRefs = instances.asScala.filterNot {
          case name => name.getKeyProperty("client-id").contains("restore")
        }
      }

      def adaptRange(timestamp: Long): Unit = {
        val rate = this.processRateMetric.map(m => m.metricValue().asInstanceOf[Double]).getOrElse(0.0).ceil
        val lag = lagRefs.map {
          case instance =>
            val metric = mBeanServer.getAttribute(instance, "records-lag-max").toString()
            if (metric == "-Infinity") 0.0 else metric.toDouble
        }.max.ceil
        if (lag > 0) started = true
        val rangeGoal = (lag + marginFactor * rate).asInstanceOf[Long]
        val size = queue.size
        val taskId = context.taskId()
        val toLog =
          s"adaptRange => id:$taskId rate:$rate lag:$lag queue:$size range:$range goal:$rangeGoal processed:$processed"
        if (toLog != lastAdaptLog) {
          logger.info(toLog)
          lastAdaptLog = toLog
        }
        if (started && lag == 0) range = Math.min(rangeGoal, initialRange)
        while (queue.size > range) forward()
      }

      override def transform(key: Null, value: GenericRecord): KeyValue[Null, GenericRecord] = {
        processed += 1L
        val timestamp = AvroTimestampExtractor.extractTimestamp(value).getOrElse(context.timestamp())
        val messageId = s"${context.topic()}-${context.partition()}-${context.offset()}"
        val message = new Message(timestamp, messageId)
        store.put(messageId, value)
        queue.enqueue(message)
        while (queue.size > range) forward()
        null
      }

      override def close(): Unit = {
        while (queue.nonEmpty) {
          forward()
        }
      }

      private def forward(): Unit = {
        val message = queue.dequeue()
        val event = store.get(message.id)
        context.forward(null, event, To.all().withTimestamp(message.timestamp))
      }
    }
  }
}

final case class Message(timestamp: Long, id: String)

object Message {
  val ordering = new Ordering[Message] {
    override def compare(x: Message, y: Message): Int = {
      y.timestamp.compareTo(x.timestamp) // we want a min-heap thus high priority is smallest timestamp
    }
  }
}